import {Navigate} from 'react-router-dom';

import {useContext, useEffect} from 'react';
import UserContext from '../UserContext';


export default function Logout(){


	const {unsetUser, setUser, user} = useContext(UserContext);

	useEffect(()=> {
		unsetUser();
		setUser({
			id: null,
			isAdmin: null
		});
	}, [])


	return (
		<Navigate to ='/login'/>

		)
}