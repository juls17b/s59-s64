import React, { useState, useEffect, Fragment } from 'react';
import ActiveProductsCard from '../components/ActiveProductsCard';
// import SingleProduct from '../components/SingleProduct';

   export default function ProductCatalogPage() {
    const [productsData, setProductsData] = useState([])

    useEffect(()=> {
        fetch(`${process.env.REACT_APP_API_URL}/products/activeProducts`, {
            headers: {
                'Content-Type' : 'application/json'
            }
        })
        .then(response => response.json())
        .then(data => {
            console.log(data)
            setProductsData(data.map(product=>{
                return(
                    <ActiveProductsCard key={product._id}
                        productProp={product}/>
                )
            }))
        });
    }, [])

     return (
       <Fragment>
            {productsData}
       </Fragment>
     );
   };