import { Button, Form, Row, Col } from 'react-bootstrap';
import {useState, useEffect, useContext} from 'react';
import {useNavigate} from 'react-router-dom';
import ErrorPage from './ErrorPage';
import UserContext from '../UserContext';
import Swal2 from 'sweetalert2';

export default function Register(){

	const [email, setEmail] = useState('');
	const [firstName, setFirstName] = useState('');
	const [lastName, setLastName] = useState('');
	const [mobileNo, setMobileNo] = useState('');
	const [password1, setPassword1] = useState('');
	const [password2, setPassword2] = useState('');

	const [isPassed, setIsPassed] = useState(true);

	const [isDisabled, setIsDisabled] = useState(false);


	const [isPasswordMatch, setIsPasswordMatch] = useState(true);

	const { user } = useContext(UserContext);

	const navigate = useNavigate();




	useEffect(()=> {
		if(email.length > 25){
			setIsPassed(false);
		}else {
			setIsPassed(true);
		}
	}, [email]);


	useEffect(()=>{

		if(email !== '' && password1 !== '' && password2 !== '' && password1 === password2 && email.length <= 25 && mobileNo !== '' && firstName !== '' && lastName !== ''){
			setIsDisabled(false);
		} else {
			setIsDisabled(true);
		}
	}, [email, password1, password2]);

	function registerUser(event) {
		event.preventDefault();

		fetch(`${process.env.REACT_APP_API_URL}/users/checkEmail`, {
		method: 'POST',
		headers: {
			'Content-Type' : 'application/json'
		},
		body: JSON.stringify({
			email: email
		})
		
		}).then(response =>{
			return(response.json())
		.then(data=> {
			if(data === false){
			fetch(`${process.env.REACT_APP_API_URL}/users/register`,{
				method: 'POST',
				headers: {'Content-Type' : 'application/json'},
				body: JSON.stringify({
					email: email,
					firstName: firstName,
					lastName: lastName,
					mobileNo: mobileNo,
					password: password1
				})
			})
			Swal2.fire({
				title: 'Succesfully registered!',
				icon: 'success',
				text: 'Welcome to Zuitt!'
			})
			navigate('/login');
			}else{
			Swal2.fire({
				title: 'Failed to Register!',
				icon: 'error',
				text: 'Email already exist!'
			})
			}
		})
		})
	}

	useEffect(()=> {
		(password1 !== password2) ? setIsPasswordMatch(false) : setIsPasswordMatch(true)
	}, [password1, password2])

	return(
		user.id === null || user.id === undefined
		?
		<Row>
			<Col className="col-6 mx-auto">
				<h1 className="text-center mt-3">Register</h1>
				<Form>
				      <Form.Group className="mb-3" controlId="formBasicEmail">
				        <Form.Label>Email address</Form.Label>
				        <Form.Control type="email" placeholder="Enter email" value= {email} onChange={(event) => setEmail(event.target.value)} />
				        <Form.Text className="text-danger" hidden={isPassed}>
				          The email should not exceed 25 characters!
				        </Form.Text>
				      </Form.Group>

				      <Form.Group className="mb-3" controlId="formFirstName">
				        <Form.Label>First Name</Form.Label>
				        <Form.Control type="text" placeholder="First Name" value= {firstName} onChange={(event) => setFirstName(event.target.value)} />
				      </Form.Group>

				      <Form.Group className="mb-3" controlId="forLastName">
				        <Form.Label>Last Name</Form.Label>
				        <Form.Control type="text" placeholder="Last Name" value= {lastName} onChange={(event) => setLastName(event.target.value)} />
				      </Form.Group>

				      <Form.Group className="mb-3" controlId="formMobileNo">
				        <Form.Label>Mobile Number</Form.Label>
				        <Form.Control type="tel" placeholder="First Name" value= {mobileNo} onChange={(event) => setMobileNo(event.target.value)} />
				      </Form.Group>

				      <Form.Group className="mb-3" controlId="formBasicPassword1">
				        <Form.Label>Password</Form.Label>
				        <Form.Control type="password" placeholder="Password" 
				        value={password1}
				        onChange={event=>setPassword1(event.target.value)}/>
				      </Form.Group>

				      <Form.Group className="mb-3" controlId="formBasicPassword2">
				        <Form.Label>Confirm Password</Form.Label>
				        <Form.Control type="password" placeholder="Retype your nominated password" value={password2} onChange={event=>setPassword2(event.target.value)}/>
				         <Form.Text className="text-danger" hidden={isPasswordMatch}>
				          The passwords does not match!
				        </Form.Text>
				      </Form.Group>

				      <Button variant="primary" onClick={event => registerUser(event)} disabled= {isDisabled}>
				        Sign up
				      </Button>
				</Form>
			</Col>
		</Row>
		:
		<ErrorPage/>
	)
}