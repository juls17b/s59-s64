import { Fragment } from 'react';

export default function Home(){
	return (
		<Fragment>
			<div className="App">
      			<header className="App-header">
        			<h1 className='text-white'>Welcome to Helmet Shop</h1>
        			<p>Provides safety to everyone.</p>
      			</header>
      			
    		</div>
		</Fragment>
	)
}