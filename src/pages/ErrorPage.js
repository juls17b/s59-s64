import {Navigate, Link} from 'react-router-dom';

export default function ErrorPage() {
	return (
		<>
		<h1 className="pt-5">Page Not Found</h1>
		<h4 className="text-muted">Go back to the <Link as = {Navigate} to = '/'>homepage</Link> </h4>
		</>
	)
}