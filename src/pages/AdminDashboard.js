import { Fragment } from 'react';
import ADBanner from '../components/ADBanner';
import ADProductTable from '../components/ADProductTable';

export default function AdminDashboard(){
	return (
		<Fragment>
			<ADBanner/>
            <ADProductTable/>
		</Fragment>
	)
}