import { Button, Form, Row, Col } from 'react-bootstrap';
import { useState, useEffect, useContext } from 'react';
import {Navigate, useNavigate} from 'react-router-dom';
import UserContext from '../UserContext';

import Swal2 from 'sweetalert2';

export default function Login(){

	const navigate = useNavigate();

	const [email, setEmail] = useState('');
	const [password, setPassword] = useState('');

	const [isHiddenEmail, setIsHiddenEmail] = useState(true);
	const [isHiddenPassword, setIsHiddenPassword] = useState(true);
	const [isDisabled, setIsDisabled] = useState(false);

    const { user, setUser} = useContext(UserContext);

	useEffect(()=> {

    }, [])

	useEffect(()=> {
		if(email){
			setIsHiddenEmail(true);
		} else {
			setIsHiddenEmail(false);
		}
	});

	useEffect(()=> {
		if(password){
			setIsHiddenPassword(true);
		} else {
			setIsHiddenPassword(false);
		}
	});

	useEffect(()=> {
		if(email !== '' && password !== ''){
			setIsDisabled(false);
		} else {
			setIsDisabled(true);
		}
	}, [email, password]);

	function loginUser(event) {
		event.preventDefault();
		

		fetch(`${process.env.REACT_APP_API_URL}/users/login`, {
			method: 'POST',
			headers: {
				'Content-Type' : 'application/json'
			},
			body: JSON.stringify({
				email: email,
				password: password
			})
		}).then(response =>{
		return(response.json())}).then(data => {
			console.log(data.adminAccess)
			if(data === false){
			Swal2.fire({
				title: "Login unsuccessfully!",
				icon: 'error',
				text: 'Check your login credentials and try again'
			})
			}else{
				if(data.customerAccess){
                	localStorage.setItem('token', data.customerAccess)
					retrieveUserDetails(data.customerAccess)
                	Swal2.fire({
                    title: "Login successfully!",
                    icon: 'success',
                    text: 'Welcome to Zuitt!'
                	});
                	navigate('/productCatalogPage');
                	console.log(data);
                } 
				
                else if(data.adminAccess){
                    localStorage.setItem('token', data.adminAccess)
                    retrieveUserDetails(data.adminAccess)
                    Swal2.fire({
                        title: "Admin Login successfully!",
                        icon: 'success',
                        text: 'Welcome to Admin Dashboard!'
                    });
                    navigate('/adminDashboard');
                    console.log(data);
                
                }
            }
		})

	};

	const retrieveUserDetails = (token) => {
		fetch(`${process.env.REACT_APP_API_URL}/users/getProfile`, {
			headers: {
				Authorization: `Bearer ${token}`
			}
		})
		.then(response => response.json())
		.then(data=> {
			setUser({
				id: data._id,
				isAdmin: data.isAdmin
			})
			console.log(data)
		})
	}

	return (
	user.id === null || user.id === undefined
	?
	<Row>
		<Col className="col-6 mx-auto">
			<h1 className="mt-3 text-center">Login</h1>
			<Form onSubmit={event=> loginUser(event)}>
			      <Form.Group className="mb-3" controlId="formBasicEmail">
			        <Form.Label>Email address</Form.Label>
			        <Form.Control type="email" placeholder="Enter email" value={email} onChange={event=> setEmail(event.target.value)}/>
			        <Form.Text className="text-danger" hidden={isHiddenEmail}>
			          Please input valid Email
			        </Form.Text>
			      </Form.Group>

			      <Form.Group className="mb-3" controlId="formBasicPassword">
			        <Form.Label>Password</Form.Label>
			        <Form.Control type="password" placeholder="Password" value={password} onChange={event=> setPassword(event.target.value)}/>
			        <Form.Text className="text-danger" hidden={isHiddenPassword}>
			        Please input your Password
			      </Form.Text>
			      </Form.Group>
			      
			      
			      <Button variant="success" id="submitBtn" type="submit" disabled={isDisabled}>
			        Login
			      </Button>
			    </Form>
		</Col>
	</Row>
  	:
	<Navigate to= ''/>
	)
};