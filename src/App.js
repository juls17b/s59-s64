import AppNavBar from './components/AppNavbar';
import './App.css';
import {BrowserRouter, Route, Routes} from 'react-router-dom';
import { Container } from 'react-bootstrap';
import {UserProvider} from './UserContext';
import {useState, useEffect} from 'react';
import Login from './pages/Login';
import Register from './pages/Register';
import Logout from './pages/Logout';
import AdminDashboard from './pages/AdminDashboard';
import Home from './pages/Home';
import ProductCatalogPage from './pages/ProductCatalogue';
import SingleProduct from './components/SingleProduct';

function App() {

  const [user, setUser] = useState({
    id: null,
    isAdmin: null
  });

  const [cart, setCart] = useState();

  const unsetUser = () => {
    localStorage.clear();
  }

  useEffect(()=> {
    console.log(user)
  }, [user]);

  useEffect(()=> {
    
  })

  useEffect(()=> {
    fetch(`${process.env.REACT_APP_API_URL}/users/getProfile`, {
      headers: {
        Authorization: `Bearer ${localStorage.getItem('token')}`
      }
    })
    .then(response => response.json())
    .then(data => {
      setUser({
      id: data._id,
      isAdmin: data.isAdmin 
      })
    })
  }, [])

  return (
    <UserProvider value = {{user, setUser, unsetUser}}>
      <BrowserRouter>
        <AppNavBar/>
          <Container>
            <Routes>
              <Route path = '/login' element = {<Login/>}/>
              <Route path = '/register' element = {<Register/>}/>
              <Route path = '/logout' element = {<Logout/>}/>
              <Route path = '/adminDashboard' element ={<AdminDashboard/>}/>
              <Route path = '/' element = {<Home/>}/>
              <Route path = '/productCatalogPage' element = {<ProductCatalogPage/>}/>
              <Route path = '/singleProduct/:id' element = {<SingleProduct/>}/>
            </Routes>
          </Container>
      </BrowserRouter>
    </UserProvider>
  );
}

export default App;
