import Container from 'react-bootstrap/Container';
import Navbar from 'react-bootstrap/Navbar';
import Nav from 'react-bootstrap/Nav';
import {NavLink, Link} from 'react-router-dom';
import UserContext from '../UserContext';
import {useContext} from 'react';

export default function AppNavBar() {

    const { user } = useContext(UserContext);
    // console.log(user)
    return (
		<Navbar bg="info" expand="lg">
            <Container fluid>
                <Navbar.Brand as = {Link} to = '/'>
                    <img
                        src="https://cdn-icons-png.flaticon.com/512/838/838907.png?w=826&t=st=1689766532~exp=1689767132~hmac=69ad7bc7235115d93b4b46856e7a330164484aa6b376289c3e2d7ef9436faaf8"
                        width="30"
                        height="30"
                        className='d-inline-block align-top'
                        alt="Logo"
                    />
                </Navbar.Brand>
                <Navbar.Toggle aria-controls="basic-navbar-nav"/>
                <Navbar.Collapse id="basic-navbar-nav">
                    <Nav className="ms-auto">
                    {       
                        user.isAdmin === true
                        ?
                        <Nav.Link as = {NavLink} to = '/adminDashboard'>Dashboard</Nav.Link>
                        :
                        <Nav.Link as = {NavLink} to = '/productCatalogPage'>Products</Nav.Link>
                    }
                    
                    {
                        user.id === null || user.id === undefined
                        ?
                        <>
                            <Nav.Link as = {NavLink} to = '/login'>Login</Nav.Link>
                            <Nav.Link as = {NavLink} to = '/register'>Register</Nav.Link>
                        </>
                        :
                        <>
                            <Nav.Link as = {NavLink} to = '/userCart'>Cart</Nav.Link>
                            <Nav.Link as = {NavLink} to = '/logout'>Logout</Nav.Link>
                        </>
                    }

                    
                    </Nav>
                </Navbar.Collapse>
            </Container>
        </Navbar>
		)
}