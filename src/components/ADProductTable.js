// import {useTable} from 'react-table';
import Table from 'react-bootstrap/Table';
import { useState, useEffect } from 'react';
import Button from 'react-bootstrap/Button';
import Form from 'react-bootstrap/Form';
import Modal from 'react-bootstrap/Modal';
import Swal2 from 'sweetalert2';

export default function ADProductTable(){
    const [products, setProducts] = useState([]);
    const [show, setShow] = useState(false);
    const handleClose = () => setShow(false);
    const handleShow = () => setShow(true);
    const [productId, setProductId] = useState('');
    const [productName, setProductName] = useState('');
    const [description, setDescription] = useState('');
    const [price, setPrice] = useState('');
    const [quantity, setQuantity] = useState('');
    // const [isActive, setIsActive] = useState();
    const [updateButton, setUpdateButton] = useState(false);
    const [formData, setFormData] = useState('');
    const [toArchive, setToArchive] = useState(products._id);
    const [toEnable, setToEnable] = useState(products._id);
    // const [availability, setAvailability] = useState('');
    
    
    function fetchProducts() {
        fetch(`${process.env.REACT_APP_API_URL}/products/allProducts`, {
            headers: {
                Authorization: `Bearer ${localStorage.getItem('token')}`,
                'Content-Type' : 'application/json'
            }
        })
        .then(response => response.json())
        .then(data => {
            // console.log(data)
            setProducts(data)
        });
        
    }

    useEffect(()=> {
        fetchProducts();
    }, [products])

    useEffect(()=> {
        if(productId !== '' && productName !== '' && description !== '' && price !== '' && quantity !== ''){
            setUpdateButton(false)
        } else {
            setUpdateButton(true)
        }
    }, [])
    

    function updateProduct(event) {
        event.preventDefault();
        console.log(event)
        fetch(`${process.env.REACT_APP_API_URL}/products/${productId}`, {
            method: 'PATCH',
            headers: {
                Authorization: `Bearer ${localStorage.getItem('token')}`,
                'Content-Type' : 'application/json'
            },
            body: JSON.stringify({
                productName: productName,
                description: description,
                price: price,
                quantity: quantity
            })
        })
        .then(response => response.json())
        .then(data => {
            console.log(data)
			if(data === false){
			Swal2.fire({
                customClass: {
                    container: 'my-swal'
                },
                title: 'Failed to Update Product',
				icon: 'error',
				text: 'Product Existed'
			})
			}else {
			Swal2.fire({
                customClass: {
                    container: 'my-swal'
                },
				title: 'Succesful',
				icon: 'success',
				text: 'Product Updated!'
			})
            handleClose();
			}
        });
    }
    
    function archiveProduct(toArchive) {
        fetch(`${process.env.REACT_APP_API_URL}/products/${toArchive}/archive`, {
            method: 'PATCH',
            headers: {
                Authorization: `Bearer ${localStorage.getItem('token')}`,
                'Content-Type' : 'application/json'
            }
        })
        .then(response => response.json())
        .then(data => {
            console.log(data)
			if(data === false){
			Swal2.fire({
                customClass: {
                    container: 'my-swal'
                },
                title: 'Failed to Disable Product',
				icon: 'error',
				text: 'Product Existed'
			})
			}else {
			Swal2.fire({
                customClass: {
                    container: 'my-swal'
                },
				title: 'Succesful',
				icon: 'success',
				text: 'Product Disabled!'
			})
            handleClose();
			}
        });
    }

    // useEffect(()=> {
    //     if(toArchive){
    //         archiveProduct(toArchive)
    //     } else {
    //         enableProduct(toEnable)
    //     }
    // }, [])

    function enableProduct(toEnable) {
        fetch(`${process.env.REACT_APP_API_URL}/products/${toEnable}/enableProduct`, {
            method: 'PATCH',
            headers: {
                Authorization: `Bearer ${localStorage.getItem('token')}`,
                'Content-Type' : 'application/json'
            }
        })
        .then(response => response.json())
        .then(data => {
            console.log(data)
			if(data === false){
			Swal2.fire({
                customClass: {
                    container: 'my-swal'
                },
                title: 'Failed to Enable Product',
				icon: 'error',
				text: 'Product Existed'
			})
			}else {
			Swal2.fire({
                customClass: {
                    container: 'my-swal'
                },
				title: 'Succesful',
				icon: 'success',
				text: 'Product Enabled!'
			})
            handleClose();
			}
        });
    }

    // useEffect(()=> {
    //     if(toEnable){
    //         enableProduct(toEnable)
    //     } else {
    //         console.log(toEnable)
    //     }
    // }, [])

    return (
        <div>
            <h1 className='pt-5'>Helmets</h1>
            <Table striped bordered hover className='table-container'>
                <thead className='t-head-color'>
                    <tr>
                        <th className='id-cell'>ID</th>
                        <th>Product Name</th>
                        <th className='th-lg'>Description</th>
                        <th>Price</th>
                        <th>Quantity</th>
                        <th>Status</th>
                        <th>Created At</th>
                        <th>Actions</th>
                    </tr>
                </thead>
                <tbody className='td'>
                    {products.map((products)=> 
                        <tr key={products._id} className='td'>
                            <td className='id-cell p-1 text-truncate'>{products._id}</td>
                            <td className='text-center p-1'>{products.productName}</td>
                            <td className='th-lg p-1'>{products.description}</td>
                            <td className='text-center p-1'>{products.price}</td>
                            <td className='text-center p-1'>{products.quantity}</td>
                            <td className='text-center p-1'>{products.isActive ? 'Available' : "Not Available"}</td>
                            <td className='text-center p-1'>{products.createdOn}</td>

                            <td><Button className="my-button" variant="primary" onClick={handleShow}>
                                Update
                            </Button>

                            <Modal show={show} onHide={handleClose}>
                                <Modal.Header closeButton>
                                    <Modal.Title>Update a Product</Modal.Title>
                                </Modal.Header>
                            <Modal.Body>
                                <Form onChange={event => setFormData(event.target.value)}>
                                    <Form.Group className="mb-3" controlId="productId">
                                            <Form.Label>Product Id</Form.Label>
                                            <Form.Control
                                                type="text"
                                                value={productId}
                                                onChange={event => setProductId(event.target.value)}
                                                autoFocus
                                            />
                                    </Form.Group>
                                    <Form.Group className="mb-3" controlId="productName">
                                            <Form.Label>Product Name</Form.Label>
                                            <Form.Control
                                                type="text"
                                                value={productName}
                                                onChange={event => setProductName(event.target.value)}
                                                autoFocus
                                            />
                                    </Form.Group>
                                    <Form.Group className="mb-3" controlId="description">
                                            <Form.Label>Description</Form.Label>
                                            <Form.Control
                                                type="text"
                                                value={description}
                                                onChange={event => setDescription(event.target.value)}
                                                autoFocus
                                            />
                                    </Form.Group>
                                    <Form.Group className="mb-3" controlId="number">
                                            <Form.Label>Price</Form.Label>
                                            <Form.Control
                                                type="number"
                                                value={price}
                                                onChange={event => setPrice(event.target.value)}
                                                autoFocus
                                            />
                                    </Form.Group>
                                    <Form.Group className="mb-3" controlId="quantity">
                                            <Form.Label>Quantity</Form.Label>
                                            <Form.Control
                                                type="number"
                                                value={quantity}
                                                onChange={event => setQuantity(event.target.value)}
                                                autoFocus
                                            />
                                    </Form.Group>
                                </Form>
                            </Modal.Body>
                            <Modal.Footer>
                                <Button variant="secondary" onClick={handleClose}>
                                    Close
                                </Button>
                                <Button variant="primary" disabled={updateButton} onClick={event => updateProduct(event)} type='submit' value={formData}>
                                    Save Changes
                                </Button> 
                            </Modal.Footer>
                        </Modal>
                            <Button className='disableButton' onClick={() => archiveProduct(`${products._id}`)} disabled={!products.isActive}>Disable</Button>
                            <Button className='enableButton' onClick={() => enableProduct(`${products._id}`)} disabled={products.isActive}>Enable</Button>
                            
                        </td>
                        </tr>
                    )}
                </tbody>
            </Table>
        </div>
    )
}