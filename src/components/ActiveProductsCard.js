// import React, { useState, useEffect } from 'react';
import { Col, Row, Card, Button } from 'react-bootstrap';
// import Swal2 from 'sweetalert2';
import { Link } from 'react-router-dom';

export default function ProductList({productProp}){
    const {_id, productName, description, price, quantity} = productProp;
    
    return (
        <div className='d-flex justify-content-center align-items-center'>
            <Card style={{ width: '18rem' }} className='card-container box'>
                <Card.Body>
                    <Card.Title>{productName}</Card.Title>
                    <Card.Text>{description}</Card.Text>
                    <Card.Text>₱{price}</Card.Text>
                    <Card.Text>{quantity}</Card.Text>
                    <Button variant='primary' as={Link} to={`/singleProduct/${_id}`}>details</Button>
                </Card.Body>
            </Card>
        </div>
    );
};