import { useEffect, useState } from 'react';
import Button from 'react-bootstrap/Button';
import Modal from 'react-bootstrap/Modal';
import Table from 'react-bootstrap/Table';

export default function MyVerticallyCenteredModal(props) {
    const [userOrdersData, setUserOrdersData] = useState([]);

    useEffect(()=> {
        openShowOrdersModal();
    }, [])

    function openShowOrdersModal() {
        fetch(`${process.env.REACT_APP_API_URL}/products/allProducts`, {
            headers: {
                Authorization: `Bearer ${localStorage.getItem('token')}`,
                'Content-Type' : 'application/json'
            }
        })
        .then(response => response.json())
        .then(data => {
            // console.log(data)
            setUserOrdersData(data);
        });
    }
    return (
      <Modal
        {...props}
        size="lg"
        aria-labelledby="contained-modal-title-vcenter"
        centered
      >
        <Modal.Header closeButton>
          <Modal.Title id="contained-modal-title-vcenter">
            All Users Orders
          </Modal.Title>
        </Modal.Header>
        <Modal.Body>
          <Table striped bordered hover className='table-container'>
                <thead className='t-head-color'>
                    <tr>
                        <th>User Id</th>
                        <th>Product Ordered</th>
                    </tr>
                </thead>
                <tbody className='td'>
                    {userOrdersData.map((renderedData)=> 
                        <tr key={renderedData._id} className='td'>
                            <td className='id-cell p-1'>{renderedData.userId}</td>
                            <td className='text-center p-1'>{renderedData._id}</td>
                        </tr>
                    )}
                </tbody>
            </Table>
        </Modal.Body>
        <Modal.Footer>
          <Button onClick={props.onHide}>Close</Button>
        </Modal.Footer>
      </Modal>
    );
  }
  
  