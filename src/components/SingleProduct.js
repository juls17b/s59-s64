import React from 'react';
import { useParams } from 'react-router-dom';
import { useEffect, useState } from 'react';
import {Row, Col, Button, Card} from 'react-bootstrap';
import Swal2 from 'sweetalert2';

   export default function SingleProduct(){
    const [productName, setProductName] = useState('');
	const [description, setDescription] = useState('');
	const [price, setPrice] = useState('');
	const [quantity, setQuantity] = useState('');

    const { id } = useParams();
	
	// console.log(id);
	useEffect(()=> {
		fetch(`${process.env.REACT_APP_API_URL}/products/${id}`)
		.then(response => response.json())
		.then(data => {
			// console.log(data);
			setProductName(data.productName);
			setDescription(data.description);
			setPrice(data.price);
            setQuantity(data.quantity);
		})
	}, [])

    // const checkout = (id) => {
	// 	fetch(`${process.env.REACT_APP_API_URL}/users/checkout`,{
	// 		method: "POST",
	// 		headers: {
	// 			'Content-Type': 'application/json',
	// 			'Authorization': `Bearer ${localStorage.getItem('token')}`
	// 		},
	// 		body: JSON.stringify({  
    //             productId: `${id}`
	// 		})
	// 	})
	// 	.then(response => response.json())
	// 	.then(data => {
	// 		console.log(data)
	// 		data === true
	// 		?
	// 		Swal2.fire({
	// 			title: 'Checkout successful!',
	// 			icon: 'success',
	// 			text: 'Product is now on process by the seller!'
	// 		})
	// 		:
	// 		Swal2.fire({
	// 			title: 'Checkout unsuccessful!',
	// 			icon: 'error',
	// 			text: 'Please try again!'
	// 		})
	// 	})
	// }
    // const [availableStock, setAvailableStock] = useState(quantity);

    const [isDisabled, setIsDisabled] = useState(false);
	const [counterDecDisabled, setCounterDecDisabled] = useState(false);
	const [counterIncDisabled, setCounterIncDisabled] = useState(false);
	const [counter, setCounter] =  useState(0);

	const increase = () => {
		if(counter <= quantity - 1){
			setCounter(counter => counter + 1);
			setCounterDecDisabled(false);
		}else{
			setCounterIncDisabled(true);
		}
		
	};
	const decrease = () => {
		if(counter <= 0){
			setCounterDecDisabled(true);
		}else{
			setCounter(counter => counter - 1);
			setCounterIncDisabled(false);
		}
		
	};

    function helmetStock(){
        if(quantity === 1){
            setQuantity(quantity - counter)
            Swal2.fire({
				title: 'Out of Stock!',
				icon: 'error',
				text: 'Please try again later!'
			})
        } else {
            setQuantity(quantity - counter)
            Swal2.fire({
				title: 'Success!',
				icon: 'success',
				text: 'Your product is successfully added to your cart!'
			})
        }
    } 

    useEffect(()=> {
		if(quantity === 0){
			setIsDisabled(true);
		}
	}, [quantity]);


     return (
        <Row>
		<Col>
			<Card>
		     <Card.Body>
                <Card.Img variant='top' />
		        <Card.Title>{productName}</Card.Title>
		        <Card.Text>
		          {description}
		        </Card.Text>
		        <Card.Text>
		          Price: {price}
		        </Card.Text>
                <Card.Text>
		          Quantity: {quantity}
		        </Card.Text>
				<Button onClick={increase} disabled={counterIncDisabled}>+</Button>
				<span>{counter}</span>
				<Button onClick={decrease} disabled={counterDecDisabled}>-</Button>
		        <Card.Text>
				<Button variant="primary" value={quantity} disabled={isDisabled} onClick = {()=> helmetStock()}>Add to Cart</Button>
				</Card.Text>
		     </Card.Body>
			</Card>
		</Col>
	</Row>
     );
   };