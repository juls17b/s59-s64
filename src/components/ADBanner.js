import { Button, Row, Col, } from 'react-bootstrap';
import Form from 'react-bootstrap/Form';
import Modal from 'react-bootstrap/Modal';
import { useContext, useEffect, useState } from 'react';
import {Link} from 'react-router-dom';
import Swal2 from 'sweetalert2';
import UserContext from '../UserContext';
import ErrorPage from '../pages/ErrorPage';
import ShowOrdersModal from './ShowOrdersModal';

export default function Banner(){
    const [productName, setProductName] = useState('');
    const [description, setDescription] = useState('');
    const [price, setPrice] = useState('');
    const [quantity, setQuantity] = useState('');
    const [addButton, setAddButton] = useState(false);
    const {user} = useContext(UserContext);

    const [show, setShow] = useState(false);
    const handleClose = () => setShow(false);
    const handleShow = () => setShow(true);
    // const [userOrders, setUserOrders] = useState('');
    const [modalShow, setModalShow] = useState(false);

    function addNewProduct(event) {
		event.preventDefault();

		fetch(`${process.env.REACT_APP_API_URL}/products/embed`, {
		method: 'POST',
		headers: {
            Authorization: `Bearer ${localStorage.getItem('token')}`,
            'Content-Type' : 'application/json'
		},
		body: JSON.stringify({
			productName: productName,
            description: description,
            price: price,
            quantity: quantity
		})
		
		}).then(response =>{
			return(response.json())
		.then(data=> {
            console.log(data)
			if(data === false){
			Swal2.fire({
                customClass: {
                    container: 'my-swal'
                },
                title: 'Failed to Add Product',
				icon: 'error',
				text: 'Product Existed'
			})
			}else {
			Swal2.fire({
                customClass: {
                    container: 'my-swal'
                },
				title: 'Succesful',
				icon: 'success',
				text: 'Product Added!'
			})
            handleClose();
			}
		})
		})
	}

    useEffect(()=> {
        if(productName !== '' && description !== '' && price !== null && quantity !== null){
            setAddButton(false)
        } else {
            setAddButton(true)
        }
    })

    return (
        user.isAdmin === true
        ?
        <Row className='mr-auto'>
            <Col className='pt-4'>
            <h1 className='pb-3 pt-5'>Admin Dashboard</h1>
            <Button variant="primary" onClick={handleShow}>
            Add New Product
            </Button>
        <Modal show={show} onHide={handleClose}>
            <Modal.Header closeButton>
                <Modal.Title>Add New Product</Modal.Title>
            </Modal.Header>
            <Modal.Body>
                <Form>
                    <Form.Group className="mb-3" controlId="productName">
                    <Form.Label>Product Name</Form.Label>
                    <Form.Control
                    type="text"
                    value={productName}
                    onChange={event => setProductName(event.target.value)}
                    autoFocus
                    />
                    </Form.Group>
            <Form.Group className="mb-3" controlId="description">
              <Form.Label>Description</Form.Label>
              <Form.Control
                type="text"
                value={description}
                onChange={event => setDescription(event.target.value)}
                autoFocus
              />
            </Form.Group>
            <Form.Group className="mb-3" controlId="price">
              <Form.Label>Price</Form.Label>
              <Form.Control
                type="number"
                value={price}
                onChange={event => setPrice(event.target.value)}
                autoFocus
              />
            </Form.Group>
            <Form.Group className="mb-3" controlId="quantity">
              <Form.Label>Quantity</Form.Label>
              <Form.Control
                type="number"
                value={quantity}
                onChange={event => setQuantity(event.target.value)}
                autoFocus
              />
            </Form.Group>
          </Form>
        </Modal.Body>
            <Modal.Footer>
            <Button variant="secondary" onClick={handleClose}>
                Close
            </Button>
            <Button variant="primary" onClick={event => addNewProduct(event)} disabled={addButton}>
                Add Product
            </Button>
            </Modal.Footer>
        </Modal> <Button className='showUserOrdersBtn' variant="primary" onClick={() => setModalShow(true)}>
                Show User Orders
                </Button>

                <ShowOrdersModal
                show={modalShow}
                onHide={() => setModalShow(false)}
                />
            </Col>
        </Row>
        :
        <ErrorPage/>       
        )
    
}